package com.hw.droid.hwcatalog;

import hwdroid.app.HWListActivity;

import java.util.Arrays;
import java.util.List;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

public class SlidingLeftItemListViewActivity extends HWListActivity {
    private ListView mListview;
    private SlidingLeftItemAdapter mAdapter;
    private static final String[] arrStr = {
            "First", "Second", "Third", "Firth", "Fifth", "Sixth", "Seventh", "Eighth", "Ninth", "Tenth"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.showBackKey(true);
        
        mListview = this.getListView();
        List<String> list = (List<String>) Arrays.asList(arrStr);
        mAdapter = new SlidingLeftItemAdapter(this, list);
        mListview.setAdapter(mAdapter);

        registerForContextMenu(mListview);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterForContextMenu(mListview);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                Toast.makeText(this, "menu 1", Toast.LENGTH_SHORT).show();
                break;
            case 2:
                Toast.makeText(this, "menu 2", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }

        return super.onContextItemSelected(item);
    }
}

