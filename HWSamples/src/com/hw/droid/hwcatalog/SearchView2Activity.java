package com.hw.droid.hwcatalog;

import hwdroid.app.HWActivity;
import hwdroid.widget.ItemAdapter;
import hwdroid.widget.ActionBar.ActionBarView;
import hwdroid.widget.ActionBar.ActionBarView.OnBackKeyItemClick;
import hwdroid.widget.item.Item;
import hwdroid.widget.item.TextItem;
import hwdroid.widget.searchview.SearchView;
import hwdroid.widget.searchview.SearchView.SearchViewListener;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class SearchView2Activity extends HWActivity {
    private ActionBarView mActionBarView;
    
    private Toast mToast;
    
    ListView mListView;
    List<Item> mItems;
    ItemAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActivityContentView(R.layout.activity_searchview);
        mActionBarView = (ActionBarView) findViewById(R.id.actionbarview);
        mActionBarView.showBackKey(true, new OnBackKeyItemClick() {
            @Override
            public void onBackKeyItemClick() {
                SearchView2Activity.this.finish();
            }
        });
        
        mActionBarView.setTitle("SearchView Activity");
        
        mToast = new Toast(this);
        mListView = (ListView)findViewById(R.id.list); 
        mItems = new ArrayList<Item>(); 
        mAdapter = new ItemAdapter(this, mItems);
        
        for(int i = 0; i < 30; i++) {
            mAdapter.add(new TextItem("search item."));
        } 
         
        //show search view
        long start = System.currentTimeMillis();
        final SearchView searchview = new SearchView(this);
        Log.i("yingchn", "consume time : " + (System.currentTimeMillis() - start));
        searchview.setAnchorView(mActionBarView);
        searchview.setSearchViewListener(new SearchViewListener(){

            @Override
            public void startOutAnimation(int time) {
                //mActionBarView.setVisibility(View.VISIBLE);
            }

            @Override
            public void startInAnimation(int time) {
                //mActionBarView.setVisibility(View.GONE);
            }

            @Override
            public void doTextChanged(CharSequence s) {
                
                mAdapter.clear();
                
                if(s != null && s.length() > 0) {
                    
                    for(int i = 0; i < 30; i++) {
                        mAdapter.add(new TextItem(s.toString()));
                    }
                   
                } else {
                    for(int i = 0; i < 30; i++) {
                        mAdapter.add(new TextItem("search item."));
                    } 
                }
                
                mAdapter.notifyDataSetChanged();
                
            }});
        searchview.setDomainText("北京啊北京的说法");
        searchview.setOnDomainClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				searchview.setDomainText("北京市");
				
			}
		});
        
        mListView.addHeaderView(searchview);
        mListView.setAdapter(mAdapter);
    }
    
    private void showToast(CharSequence text) {
        TextView t = new TextView(this);
        t.setBackgroundColor(Color.RED);
        t.setText(text);
        t.setTextColor(Color.BLACK);
        mToast.setView(t);   
        mToast.setDuration(Toast.LENGTH_SHORT);
        mToast.setGravity(Gravity.TOP, 0, 0);
        mToast.show();
    }
}
